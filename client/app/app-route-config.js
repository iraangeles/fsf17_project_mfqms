(function () {
    angular
        .module("MyApp")
        .config(myAppConfig);

    myAppConfig.$inject = ["$stateProvider","$urlRouterProvider"];

    function myAppConfig($stateProvider,$urlRouterProvider){
        $stateProvider
            .state('page1',{
                url : '/page1',
                templateUrl: "pages/page1.html",
                controller : 'Page1Ctrl',
                controllerAs : 'ctrl'
            })
            .state('page2',{
                url : '/page2',
                templateUrl: "pages/page2.html",
                controller : 'Page2Ctrl',
                controllerAs : 'ctrl'
            })


         $urlRouterProvider.otherwise("/page1");
    
    }

})();           